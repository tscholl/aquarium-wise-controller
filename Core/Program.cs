﻿namespace AquariumWiseController.Core
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.Threading;

    using AquariumWiseController.Core.Extensions;
    using AquariumWiseController.Core.Extensions.Telnet;
    using AquariumWiseController.Core.Plugins;

    using Microsoft.SPOT.Hardware;

    using SecretLabs.NETMF.Hardware.NetduinoPlus;

    /// <summary>
    /// Delegate for Output Plugins
    /// </summary>
    /// <param name="_sender">Who sent up the data</param>
    /// <param name="_data">Data sent up from Input Plugin</param>
    public delegate void OutputPluginEventHandler(Object _sender, IPluginData _data);

    public class Program
    {
        #region Constants and Fields

        public const string ConfigFile = @"\SD\config.json";

        public const string PluginFolder = @"\SD\plugins\";

        /// <summary>
        /// Handle to attach to Input Plugin timers.  This event will be raised when an Input Plugin
        /// is processed to trigger Output Plugins to run
        /// </summary>
        private static readonly InputDataAvailable m_inputAvailable = DataAvailable;

        /// <summary>
        /// Collection of registered event handlers, mostly for dealing with web requests
        /// </summary>
        private static EventHandlerList m_eventHandlerList = new EventHandlerList();

        /// <summary>
        /// Scheduler for Plugin tasks
        /// </summary>
        private static PluginScheduler m_pluginScheduler;

        public static ShellCore shell;

        #endregion

        #region Public Methods

        public static void Main()
        {
            // Initialize required components
            try
            {
                Bootstrap();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR - Main - " + ex.Message);
            }

            // Blink LED to show we're still responsive
            var led = new OutputPort(Pins.ONBOARD_LED, false);

            while (true)
            {
                led.Write(!led.Read());
                Thread.Sleep(500);
            }
        }

        #endregion

        #region Methods

        private static void Bootstrap()
        {
            TimeEx.SetControllerTime();

            Microsoft.SPOT.Debug.GC(true);

            m_eventHandlerList = new EventHandlerList();
            m_pluginScheduler = new PluginScheduler();

            shell = new ShellCore(new IntegratedSocket("", 23));
            shell.OnDisconnected += Shell_OnDisconnected;
            ShellCommands.Bind(shell);
            shell.Start(true);

            // Each key in 'config' is a collection of plugin types (input, output, control),
            // so pull out of the root element.
            var config = ((Hashtable)JSON.JsonDecodeFromFile(ConfigFile))["config"] as Hashtable;

            // parse each plugin type
            foreach (string pluginType in config.Keys)
            {
                ParseConfig(config[pluginType] as Hashtable, pluginType);
            }

            config = null;

            m_pluginScheduler.Start();
        }

        /// <summary>
        /// Delegate for signaling output plugins that data is available.
        /// This is called from inside each Input Plugin Callback
        /// </summary>
        /// <param name="_data">data passed up from input plugin</param>
        private static void DataAvailable(IPluginData _data)
        {
            // data should be available in the queue
            // raise the event to handle it.
            var ope = (OutputPluginEventHandler)m_eventHandlerList["OutputPlugins"];

            // walk through all available output plugins
            if (ope != null)
            {
                ope(ope, _data);
            }
        }

        /// <summary>
        /// Load the assembly dynamically from the SD Card, attach any necessary handlers, and update the web front end
        /// to provide config options 
        /// </summary>
        /// <param name="_name">Name of the Plugin</param>
        /// <param name="_type">Class of plugin</param>
        /// <param name="_config"></param>
        private static void LoadPlugin(string _name, string _type, Hashtable _config)
        {
            foreach (var type in System.Reflection.Assembly.GetExecutingAssembly().GetTypes())
            {
                if (type.FullName.IndexOf(".Plugins." + _name) > 0)
                {
                    var plugin = type.GetConstructor(new[] { typeof(object) }).Invoke(new object[] { _config });

                    switch (_type)
                    {
                        case "input":
                            var ip = (InputPlugin)plugin;
                            m_pluginScheduler.AddTask(
                                new PluginEventHandler(ip.TimerCallback),
                                m_inputAvailable,
                                ip.TimerInterval,
                                ip.TimerInterval,
                                true);
                            if (_name.Equals("pH"))
                            {
                                m_eventHandlerList.AddHandler(
                                    "OutputPlugins", (OutputPluginEventHandler)ip.EventHandler);
                            }

                            break;
                        case "output":
                            var op = (OutputPlugin)plugin;
                            m_eventHandlerList.AddHandler("OutputPlugins", (OutputPluginEventHandler)op.EventHandler);
                            break;
                        case "control":
                            var cp = (ControlPlugin)plugin;
                            foreach (DictionaryEntry item in cp.Commands())
                            {
                                m_pluginScheduler.AddTask(
                                    new PluginEventHandler(cp.ExecuteControl),
                                    item.Value,
                                    (TimeSpan)item.Key,
                                    new TimeSpan(24, 0, 0),
                                    true);
                            }
                            break;
                    }

                    return;
                }
            }
        }

        /// <summary>
        /// JSON Object contains nested components which need to be parsed down to indvidual plugin instructions.
        /// This is done recursively to load all necessary plugins
        /// </summary>
        /// <param name="_section">Current Hashtable being processed</param>
        /// <param name="_type">Plugin type being processed</param>
        /// <param name="_name">Name of Plugin being searched for</param>
        private static void ParseConfig(Hashtable _section, string _type = null, string _name = null)
        {
            foreach (string name in _section.Keys)
            {
                if (_section[name] is Hashtable)
                {
                    ParseConfig((Hashtable)_section[name], _type, name);
                }
                else
                {
                    // reached bottom of config tree, pass the Hashtable to constructors
                    if (_section["enabled"].ToString() == "true")
                    {
                        LoadPlugin(_name, _type, _section);

                        Microsoft.SPOT.Debug.GC(true);
                    }

                    return;
                }
            }
        }

        /// <summary>
        /// Triggered when the connection has been lost
        /// </summary>
        /// <param name="shellCore">Reference to the shellCore</param>
        /// <param name="remoteAddress">Hostname of the user</param>
        /// <param name="time">Timestamp of the event</param>
        static void Shell_OnDisconnected(ShellCore shellCore, string remoteAddress, DateTime time)
        {
            // Starts to listen again
            shellCore.Start();
        }

        #endregion
    }
}