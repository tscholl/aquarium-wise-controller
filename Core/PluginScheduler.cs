namespace AquariumWiseController.Core
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.Threading;

    /// <summary>
    /// Delegate that is called when a PluginTask hits it's scheduled interval
    /// </summary>
    /// <param name="_state">Any extra data that is required for the callback to process correctly</param>
    public delegate void PluginEventHandler(object _state);

    public class PluginScheduler : IDisposable
    {
        #region Constants and Fields

        /// <summary>
        /// Object for thread locking
        /// </summary>
        private static object m_Locker = new Object();

        /// <summary>
        /// Timer that does all the heavy lifting
        /// </summary>
        private static Timer m_Poller;

        /// <summary>
        /// Interval to poll plugin list for execution
        /// </summary>
        private readonly TimeSpan m_pollInterval = new TimeSpan(0, 0, 10);

        /// <summary>
        /// All tasks attached to the scheduler
        /// </summary>
        private static ArrayList m_Tasks;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Constructor
        /// </summary>
        public PluginScheduler()
        {
            m_Poller = new Timer(this.PollTasks, null, -1, -1);
            m_Tasks = new ArrayList();
        }

        ~PluginScheduler()
        {
            this.Dispose();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds a task to the scheduler
        /// </summary>
        /// <param name="_delegate">Callback to execute on matching time</param>
        /// <param name="_state">State object to pass to the Callback</param>
        /// <param name="_execute">Timespan when to execute the Callback</param>
        /// <param name="_interval">Interval when to re-execute Callback, if any</param>
        /// <param name="_continuous">Should this Task be continuously rescheduled</param>
        public void AddTask(Delegate _delegate, object _state, TimeSpan _execute, TimeSpan _interval, bool _continuous)
        {
            lock (m_Locker)
            {
                var task = new PluginTask(_delegate, _state, _continuous, _interval);
                DateTime now = DateTime.Now;
                now += _execute;
                var entry = new DictionaryEntry(now, task);
                m_Tasks.Add(entry);
            }
        }

        public void Dispose()
        {
            m_Poller.Dispose();
            m_Tasks = null;
        }

        /// <summary>
        /// Start the scheduler polling
        /// </summary>
        public void Start()
        {
            lock (m_Locker)
            {
                m_Poller.Change(this.m_pollInterval, this.m_pollInterval);
            }
        }

        /// <summary>
        /// Stop the Scheduler from Polling
        /// </summary>
        public void Stop()
        {
            lock (m_Locker)
            {
                m_Poller.Change(-1, -1);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Main polling thread
        /// </summary>
        /// <param name="_state">Any required state information</param>
        private void PollTasks(object _state)
        {
            DateTime now = DateTime.Now;

            foreach (DictionaryEntry item in m_Tasks)
            {
                var key = (DateTime)item.Key;

                if (key <= now.AddSeconds(1))
                {
                    // Execute task in Value
                    var task = (PluginTask)item.Value;
                    var callback = (PluginEventHandler)task.CallBack;

                    if (callback != null)
                    {
                        try
                        {
                            Debug.WriteLine("INFO - PollTasks - Executing Task " + callback.Target);
                            callback(task.State);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("ERROR - PollTasks - " + ex.Message);
                        }
                    }

                    if (task.Reschedule)
                    {
                        item.Key = now + task.Interval;
                        Debug.WriteLine("INFO - PollTasks - Task rescheduled for " + item.Key);
                    }
                    else
                    {
                        lock (m_Locker)
                        {
                            Debug.WriteLine("INFO - PollTasks - Removing Task");
                            m_Tasks.Remove(item);
                        }
                    }

                    Microsoft.SPOT.Debug.GC(true);
                }
            }
        }

        #endregion

        /// <summary>
        /// Required information to track Plugin Task execution
        /// Not used outside the PluginScheduler, hence protected
        /// </summary>
        protected class PluginTask
        {
            #region Constructors and Destructors

            public PluginTask(Delegate _callback, object _state, bool _reschedule, TimeSpan _interval)
            {
                this.CallBack = Delegate.Combine(this.CallBack, _callback);
                this.State = _state;
                this.Reschedule = _reschedule;
                this.Interval = _interval;
            }

            #endregion

            #region Public Properties

            public Delegate CallBack { get; set; }

            public TimeSpan Interval { get; set; }

            public Boolean Reschedule { get; set; }

            public Object State { get; set; }

            #endregion
        }
    }
}