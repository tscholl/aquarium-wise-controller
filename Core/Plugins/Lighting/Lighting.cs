namespace AquariumWiseController.Core.Plugins.Lighting
{
    using System;
    using System.Collections;

    using AquariumWiseController.Core.Extensions;
    using AquariumWiseController.Core.Hardware;

    using Microsoft.SPOT.Hardware;

    public class Lighting : ControlPlugin
    {
        private Hashtable commands;

        private bool missedExecute;

        #region Constructors and Destructors

        public Lighting(object config)
        {
            this.commands = new Hashtable();

            var configTable = (Hashtable)config;
            var channels = configTable["channels"] as ArrayList;

            this.ParseCommands(channels);
        }

        ~Lighting()
        {
            this.Dispose();
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
            this.commands = null;
        }

        public override Hashtable Commands()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Execute ChannelCommand given in state object
        /// </summary>
        /// <param name="state">DictionaryEntry object to process on execution</param>
        public override void ExecuteControl(object state)
        {
            var pwm = new Netduino.PWM(Cpu.Pin.GPIO_Pin9);

            pwm.SetDutyCycle(199, 150);
        }

        #endregion

        /// <summary>
        /// Parse JSON array of channel config and store for delayed execution
        /// </summary>
        /// <param name="configCommands">JSON formatted list of objects</param>
        private void ParseCommands(IEnumerable configCommands)
        {
            foreach (Hashtable command in configCommands)
            {
                // parse out details from config
                var channelId = Int32.Parse(command["id"].ToString());
                var timeOn = TimeEx.GetTimeSpan(command["on"].ToString(), out missedExecute);

                var channelOn = new DictionaryEntry(channelId, true);

                this.commands.Add(timeOn, channelOn);

                var channelOnMissed = this.missedExecute;

                var timeOff = TimeEx.GetTimeSpan(command["off"].ToString(), out missedExecute);
                var channelOff = new DictionaryEntry(channelId, false);

                this.commands.Add(timeOff, channelOff);

                var channelOffMissed = this.missedExecute;

                if (channelOnMissed && !channelOffMissed)
                {
                    this.ExecuteControl(channelOn);
                }
                else
                {
                    this.ExecuteControl(channelOff);
                }
            }
        }
    }
}