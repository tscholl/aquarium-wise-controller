﻿namespace AquariumWiseController.Core.Plugins.Relays
{
    using System;
    using System.Collections;
    using System.Diagnostics;

    using AquariumWiseController.Core.Extensions;
    using AquariumWiseController.Core.Hardware;

    using Microsoft.SPOT.Hardware;

    using SecretLabs.NETMF.Hardware.NetduinoPlus;

    public class Relays : ControlPlugin
    {
        #region Constants and Fields

        private Hashtable commands;

        private OutputPort enablePin = new OutputPort(Pins.GPIO_PIN_D5, true);

        private Ic74hc595 ic74Hc595 = new Ic74hc595(SPI_Devices.SPI1, Pins.GPIO_PIN_D10);

        /// <summary>
        /// Used during Command parsing to trigger immediate command execution.
        /// Example: during mid-day, the lights should be on, but a reboot of the controller will restart
        /// the Netduino, forcing a re-parse of the ini file.  
        /// The timers will be setup to turn on the next day, but they need to be on immediately as well
        /// </summary>
        private bool missedExecute;

        #endregion

        #region Constructors and Destructors

        public Relays(object config)
        {
            this.ic74Hc595.Pins[0].Write(!false);
            this.ic74Hc595.Pins[1].Write(!false);
            this.ic74Hc595.Pins[2].Write(!false);
            this.ic74Hc595.Pins[3].Write(!false);
            this.ic74Hc595.Pins[4].Write(!false);
            this.ic74Hc595.Pins[5].Write(!false);
            this.ic74Hc595.Pins[6].Write(!false);
            this.ic74Hc595.Pins[7].Write(!false);

            // parse config Hashtable and store array list of configCommands
            // the individual relays are stored in an Array List
            this.commands = new Hashtable();

            var configTable = (Hashtable)config;
            var relays = configTable["relays"] as ArrayList;

            this.ParseCommands(relays);

            this.enablePin.Write(false);
        }

        ~Relays()
        {
            this.Dispose();
        }

        #endregion

        #region Public Methods

        public override Hashtable Commands()
        {
            return this.commands;
        }

        public override void Dispose()
        {
            this.ic74Hc595 = null;
            this.commands = null;
            this.enablePin = null;
        }

        /// <summary>
        /// Execute RelayCommand given in state object
        /// </summary>
        /// <param name="state">DictionaryEntry object to process on execution</param>
        public override void ExecuteControl(object state)
        {
            // Callback received a RelayCommand struct to execute			
            var command = (DictionaryEntry)state;

            Debug.WriteLine(
                "INFO - ExecuteControl - Switching relay " + (int)command.Key + " to " + (bool)command.Value);

            this.ic74Hc595.Pins[(int)command.Key].Write(!(bool)command.Value);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Parse JSON array of relay config and store for delayed execution
        /// </summary>
        /// <param name="configCommands">JSON formatted list of objects</param>
        private void ParseCommands(IEnumerable configCommands)
        {
            foreach (Hashtable command in configCommands)
            {
                // parse out details from config
                var relayId = Int32.Parse(command["id"].ToString());
                var timeOn = TimeEx.GetTimeSpan(command["on"].ToString(), out missedExecute);
                var relayOn = new DictionaryEntry(relayId, true);

                this.commands.Add(timeOn, relayOn);

                var relayOnMissed = this.missedExecute;

                var timeOff = TimeEx.GetTimeSpan(command["off"].ToString(), out missedExecute);
                var relayOff = new DictionaryEntry(relayId, false);

                this.commands.Add(timeOff, relayOff);

                var relayOffMissed = this.missedExecute;

                if (relayOnMissed && !relayOffMissed)
                {
                    this.ExecuteControl(relayOn);
                }
                else
                {
                    this.ExecuteControl(relayOff);
                }
            }
        }

        #endregion
    }
}