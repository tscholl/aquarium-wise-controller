﻿namespace AquariumWiseController.Core.Plugins.LogFile
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Text;

    using AquariumWiseController.Core.Extensions;

    public class Logfile : OutputPlugin
    {
        #region Constants and Fields

        private readonly string m_logFile;

        #endregion

        #region Constructors and Destructors

        public Logfile(object _config)
        {
            var config = (Hashtable)_config;
            this.m_logFile = config["file-name"].ToString();
            config = null;
        }

        ~Logfile()
        {
            this.Dispose();
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
        }

        public override void EventHandler(Object sender, IPluginData data)
        {
            var sb = new StringBuilder();

            // Format string for output
            sb.Append(DateTime.Now.ToString("s"));
            sb.Append(",");
            sb.Append(data.GetValue().ToString("F"));
            sb.Append(data.DataUnits());
            sb.Append("\n");

            // take data and write it out to text
            using (var fs = new FileStream(this.m_logFile, FileMode.Append))
            {
                byte[] buf = Encoding.UTF8.GetBytes(sb.ToString());
                fs.Write(buf, 0, buf.Length);
                fs.Flush();
            }
        }

        #endregion
    }
}