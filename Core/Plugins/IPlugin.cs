namespace AquariumWiseController.Core.Plugins
{
    using System;
    using System.Collections;

    /// <summary>
    /// Delegate for Input Plugins
    /// </summary>
    /// <param name="_data">Data sent up from input plugin</param>
    public delegate void InputDataAvailable(IPluginData _data);

    public enum DataType : uint
    {
        Temperature = 1,

        pH = 2,

        Lux = 3
    }

    public enum PluginType
    {
        Input,

        Output,

        Control
    }

    public abstract class Plugin : IDisposable
    {
        #region Public Methods

        public abstract void Dispose();

        #endregion
    }

    public abstract class InputPlugin : Plugin
    {
        // Timer Intervals specified in config file

        #region Public Properties

        public abstract TimeSpan TimerInterval { get; }

        #endregion

        #region Public Methods

        public abstract void EventHandler(Object sender, IPluginData data);

        public abstract void TimerCallback(Object state);

        #endregion
    }

    public abstract class OutputPlugin : Plugin
    {
        #region Public Methods

        public abstract void EventHandler(Object sender, IPluginData data);

        #endregion
    }

    public abstract class ControlPlugin : Plugin
    {
        #region Public Methods

        public abstract Hashtable Commands();

        public abstract void ExecuteControl(Object state);

        #endregion
    }

    public interface IPluginData
    {
        #region Public Methods

        DataType DataType();

        string DataUnits();

        string GetExtValue();

        double GetValue();

        #endregion
    }
}