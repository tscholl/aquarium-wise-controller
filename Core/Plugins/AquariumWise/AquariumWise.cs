﻿namespace AquariumWiseController.Core.Plugins.AquariumWise
{
    using System;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;

    using AquariumWiseController.Core.Extensions;

    public class AquariumWise : OutputPlugin
    {
        #region Constructors and Destructors

        public AquariumWise(object _config)
        {
        }

        ~AquariumWise()
        {
            this.Dispose();
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
        }

        public override void EventHandler(object sender, IPluginData data)
        {
            string httpPost;
            string macAddress = MACAddress.Get();

            switch (data.DataType())
            {
                case DataType.Temperature:
                    httpPost = "POST /api/temperature/log HTTP/1.1\nHost: my.aquariumwise.net\nConnection: close\n";
                    break;
                case DataType.pH:
                    httpPost = "POST /api/ph/log HTTP/1.1\nHost: my.aquariumwise.net\nConnection: close\n";
                    break;
                case DataType.Lux:
                    httpPost = "POST /api/lux/log HTTP/1.1\nHost: my.aquariumwise.net\nConnection: close\n";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            httpPost += "Content-Type: application/json\nContent-Length: ";

            // Build field string from data and append to the post string
            string fieldData = "{\"Value\":" + data.GetValue() + ",\"SensorRef\":\"" + data.GetExtValue()
                               + "\",\"DeviceMac\":\"" + macAddress + "\"}";

            try
            {
                IPHostEntry hostEntry = Dns.GetHostEntry("my.aquariumwise.net");

                // Open the Socket and post the data
                // Create required networking parameters
                using (
                    var myAquariumWiseSocket = new Socket(
                        AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
                {
                    myAquariumWiseSocket.Connect(new IPEndPoint(hostEntry.AddressList[0], 80));

                    byte[] sendBytes = Encoding.UTF8.GetBytes(httpPost + fieldData.Length + "\n\n" + fieldData);

                    myAquariumWiseSocket.Send(sendBytes, sendBytes.Length, 0);

                    // Wait for a response to see what happened
                    var buffer = new Byte[256];
                    string page = String.Empty;

                    // Poll for data until 30-second timeout. Returns true for data and connection closed.
                    while (myAquariumWiseSocket.Poll(20 * 1000000, SelectMode.SelectRead))
                    {
                        // If there are 0 bytes in the buffer, then the connection is closed, or we have timed out.
                        if (myAquariumWiseSocket.Available == 0)
                        {
                            break;
                        }

                        // Zero all bytes in the re-usable buffer.
                        Array.Clear(buffer, 0, buffer.Length);

                        // Read a buffer-sized HTML chunk.
                        myAquariumWiseSocket.Receive(buffer);

                        // Append the chunk to the string.
                        page = page + new String(Encoding.UTF8.GetChars(buffer));
                    }

                    myAquariumWiseSocket.Close();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR - Plugin.AquariumWise - " + ex.Message);
            }
        }

        #endregion
    }
}