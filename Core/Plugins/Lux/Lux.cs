namespace AquariumWiseController.Core.Plugins.Lux
{
    using System;
    using System.Collections;
    using System.Diagnostics;

    using AquariumWiseController.Core.Hardware;

    public class LuxData : IPluginData
    {
        #region Constants and Fields

        private string extValue;

        private double value;

        #endregion

        #region Public Methods

        public DataType DataType()
        {
            return Plugins.DataType.Lux;
        }

        public string DataUnits()
        {
            return "LX";
        }

        public string GetExtValue()
        {
            return this.extValue;
        }

        public double GetValue()
        {
            return this.value;
        }

        public void SetExtValue(string _extValue)
        {
            this.extValue = _extValue;
        }

        public void SetValue(double _value)
        {
            this.value = _value;
        }

        #endregion
    }

    public class Lux : InputPlugin
    {
        #region Constants and Fields

        private readonly TimeSpan timerInterval;

        #endregion

        #region Constructors and Destructors

        public Lux(object _config)
        {
            var config = (Hashtable)_config;

            // The Timer Interval is specified in an Arraylist of numbers
            var interval = config["interval"] as ArrayList;

            // TODO: Double casting since for some reason an explicit cast from a Double
            // to an Int doesn't work.  It's a boxing issue, as interval[i] returns an object.
            // And JSON.cs returns numbers as doubles
            var times = new int[3];

            for (int i = 0; i < 3; i++)
            {
                times[i] = (int)(double)interval[i];
            }

            this.timerInterval = new TimeSpan(times[0], times[1], times[2]);
        }

        ~Lux()
        {
            this.Dispose();
        }

        #endregion

        #region Public Properties

        public override TimeSpan TimerInterval
        {
            get
            {
                return this.timerInterval;
            }
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
        }

        // Lux doesn't need the Output Handler
        public override void EventHandler(object sender, IPluginData data)
        {
            throw new NotSupportedException();
        }

        public override void TimerCallback(object state)
        {
            var bh1750 = new BH1750();

            var luxData = new LuxData();

            // Timer Callbacks receive a Delegate in the state object
            var ida = (InputDataAvailable)state;

            try
            {
                luxData.SetValue(bh1750.GetLux());

                ida(luxData);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR - Plugin.Lux - " + ex.Message);
            }
        }

        #endregion
    }
}