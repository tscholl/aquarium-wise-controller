﻿namespace AquariumWiseController.Core.Plugins.pH
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.IO.Ports;
    using System.Text;
    using System.Threading;

    using AquariumWiseController.Core.Extensions;
    using AquariumWiseController.Core.Extensions.Telnet;

    public class pHData : IPluginData
    {
        #region Constants and Fields

        private string extValue;

        private double value;

        #endregion

        #region Public Methods

        public DataType DataType()
        {
            return Plugins.DataType.pH;
        }

        public string DataUnits()
        {
            return "pH";
        }

        public string GetExtValue()
        {
            return this.extValue;
        }

        public double GetValue()
        {
            return this.value;
        }

        public void SetExtValue(string _extValue)
        {
            this.extValue = _extValue;
        }

        public void SetValue(double _value)
        {
            this.value = _value;
        }

        #endregion
    }

    public class pH : InputPlugin
    {
        #region Constants and Fields

        private readonly string temperatureRomId;

        private readonly TimeSpan timerInterval;

        private static bool requestFromShell;

        /// <summary>
        /// Latest temperature reading passed in from the Temperature Plugin
        /// </summary>
        private static double temperature;

        private InputDataAvailable ida;

        #endregion

        #region Constructors and Destructors

        public pH()
        {
            temperature = 0.0F;
        }

        public pH(object _config)
        {
            var config = (Hashtable)_config;

            this.temperatureRomId = config["TemperatureRomId"] as String;

            // The Timer Interval is specified in an Arraylist of numbers
            var interval = config["interval"] as ArrayList;

            // TODO: Double casting since for some reason an explicit cast from a Double
            // to an Int doesn't work.  It's a boxing issue, as interval[i] returns an object.
            // And JSON.cs returns numbers as doubles
            var times = new int[3];

            for (int i = 0; i < 3; i++)
            {
                times[i] = (int)(double)interval[i];
            }

            this.timerInterval = new TimeSpan(times[0], times[1], times[2]);

            Program.shell.OnCommandReceived += Shell_OnCommandReceived;

            com1 = new SerialPort(Serial.COM1, 38400, Parity.None, 8, StopBits.One) { ReadTimeout = Timeout.Infinite, WriteTimeout = 1000 };
            com1.DataReceived += this.Com1_DataReceived;
        }

        ~pH()
        {
            this.Dispose();
        }

        #endregion

        #region Public Properties

        public override TimeSpan TimerInterval
        {
            get
            {
                return this.timerInterval;
            }
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
        }

        /// <summary>
        /// Records the last temperature reading from the Temperature Plugin
        /// </summary>
        /// <param name="sender">Object that raised the callback</param>
        /// <param name="data">Last reading</param>
        public override void EventHandler(object sender, IPluginData data)
        {
            if (data.DataType() == DataType.Temperature && data.GetExtValue() == this.temperatureRomId)
            {
                temperature = data.GetValue();
            }
        }

        public override void TimerCallback(object state)
        {
            //Timer Callbacks receive a Delegate in the state object
            this.ida = (InputDataAvailable)state;

            GetpH();
        }

        #endregion

        #region Methods

        private static void Shell_OnCommandReceived(ShellCore shellCore, string[] arguments, ref bool suspressError, DateTime time)
        {
            switch (arguments[0].ToUpper())
            {
                case "PH":
                    requestFromShell = true;

                    GetpH();

                    while (requestFromShell)
                    {
                        //Wait for response
                    }

                    suspressError = true;
                    break;
            }
        }

        private void Com1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                var textArrived = string.Empty;

                const string Value = "\r";

                // This is for speed performance (in loops below)
                var byteValue = Encoding.UTF8.GetBytes(Value);
                var byteValueLen = byteValue.Length;

                bool flag;

                // +1 because of two byte characters
                var buffer = new byte[byteValueLen + 1];

                // Read until pattern or timeout
                do
                {
                    var bufferIndex = 0;
                    Array.Clear(buffer, 0, buffer.Length);

                    // Read data until the buffer size is less then pattern.Length
                    // or last char in pattern is received
                    do
                    {
                        var bytesRead = com1.Read(buffer, bufferIndex, 1);
                        bufferIndex += bytesRead;

                        // if nothing was read (timeout), we will return null
                        if (bytesRead <= 0)
                        {
                            return;
                        }
                    }
                    while ((bufferIndex < byteValueLen + 1) && (buffer[bufferIndex - 1] != byteValue[byteValueLen - 1]));

                    // Decode received bytes into chars and then into string
                    var charData = Encoding.UTF8.GetChars(buffer);

                    for (var i = 0; i < charData.Length; i++)
                    {
                        textArrived += charData[i];
                    }

                    flag = true;

                    // This is very important!! Bytes received can be zero-length string.
                    // For example 0x00, 0x65, 0x66, 0x67, 0x0A will be decoded as empty string.
                    // So this condition is not a burden!
                    if (textArrived.Length > 0)
                    {
                        // check whether the end pattern is at the end
                        for (var i = 1; i <= Value.Length; i++)
                        {
                            if (Value[Value.Length - i] != textArrived[textArrived.Length - i])
                            {
                                flag = false;
                                break;
                            }
                        }
                    }
                }
                while (!flag);

                // chop end pattern
                if (textArrived.Length >= Value.Length)
                {
                    textArrived = textArrived.Substring(0, textArrived.Length - Value.Length);
                }

                // Stamp can return text if reading was not successful, so test before returning
                double phReading;

                if (Parse.TryParseDouble(textArrived, out phReading))
                {
                    if (!requestFromShell)
                    {
                        var phData = new pHData();
                        phData.SetValue(phReading);

                        this.ida(phData);
                    } else
                    {
                        Program.shell.TelnetServer.Print("pH: " + phReading);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR - Plugin.pH - " + ex.Message);
            }
            finally
            {
                requestFromShell = false;
            }
        }

        private static SerialPort com1;

        private static void GetpH()
        {
            try
            {
                string command;

                if (temperature > 0)
                {
                    command = temperature.ToString("F") + "\r";
                }
                else
                {
                    command = "R\r";
                }

                var message = Encoding.UTF8.GetBytes(command);

                if (!com1.IsOpen)
                {
                    com1.Open();
                }

                com1.Write(message, 0, message.Length);
                com1.Flush();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR - Plugin.pH - " + ex.Message);
            }
        }

        #endregion
    }
}