namespace AquariumWiseController.Core.Plugins.AquariumWise
{
    using System;
    using System.Diagnostics;

    using AquariumWiseController.Core.Hardware;

    using SecretLabs.NETMF.Hardware.NetduinoPlus;

    public class LcdDisplay : OutputPlugin
    {
        private Ic74hc595 ic74Hc595 = new Ic74hc595(SPI_Devices.SPI1, Pins.GPIO_PIN_D9);

        private readonly Hd44780Lcd hd44780Lcd;

        #region Constructors and Destructors

        public LcdDisplay(object _config)
        {
            hd44780Lcd = new Hd44780Lcd(ic74Hc595.Pins[0], ic74Hc595.Pins[1], ic74Hc595.Pins[2], ic74Hc595.Pins[3], ic74Hc595.Pins[4], ic74Hc595.Pins[5]);

            hd44780Lcd.Write("Hello Aquarium Wise!");
        }

        ~LcdDisplay()
        {
            this.Dispose();
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
            this.ic74Hc595 = null;
        }

        public override void EventHandler(object sender, IPluginData data)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR - Plugin.LcdDisplay - " + ex.Message);
            }
        }

        #endregion
    }
}