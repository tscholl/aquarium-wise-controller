﻿namespace AquariumWiseController.Core.Plugins.Temperature
{
    using System;
    using System.Collections;
    using System.Threading;

    using AquariumWiseController.Core.Extensions;
    using AquariumWiseController.Core.Extensions.Telnet;
    using AquariumWiseController.Core.Hardware;

    using CW.NETMF.Hardware;

    using SecretLabs.NETMF.Hardware.NetduinoPlus;

    public class TemperatureData : IPluginData
    {
        #region Constants and Fields

        private string extValue;

        private double value;

        #endregion

        #region Public Methods

        public DataType DataType()
        {
            return Plugins.DataType.Temperature;
        }

        public string DataUnits()
        {
            return "C";
        }

        public string GetExtValue()
        {
            return this.extValue;
        }

        public double GetValue()
        {
            return this.value;
        }

        public void SetExtValue(string _extValue)
        {
            this.extValue = _extValue;
        }

        public void SetValue(double _value)
        {
            this.value = _value;
        }

        #endregion
    }

    public class Temperature : InputPlugin
    {
        #region Constants and Fields

        private readonly TimeSpan timerInterval;

        #endregion

        #region Constructors and Destructors

        public Temperature(object _config)
        {
            var config = (Hashtable)_config;

            // The Timer Interval is specified in an Arraylist of numbers
            var interval = config["interval"] as ArrayList;

            // TODO: Double casting since for some reason an explicit cast from a Double
            // to an Int doesn't work.  It's a boxing issue, as interval[i] returns an object.
            // And JSON.cs returns numbers as doubles
            var times = new int[3];

            for (var i = 0; i < 3; i++)
            {
                times[i] = (int)(double)interval[i];
            }

            this.timerInterval = new TimeSpan(times[0], times[1], times[2]);

            Program.shell.OnCommandReceived += Shell_OnCommandReceived;
        }

        ~Temperature()
        {
            this.Dispose();
        }

        #endregion

        #region Public Properties

        public override TimeSpan TimerInterval
        {
            get
            {
                return this.timerInterval;
            }
        }

        #endregion

        #region Public Methods

        public override void Dispose()
        {
        }

        // Temperature doesn't need the Output Handler
        public override void EventHandler(object sender, IPluginData data)
        {
            throw new NotSupportedException();
        }

        public IPluginData GetData()
        {
            return null;
        }

        private static ArrayList GetTemperatures()
        {
            var temperatures = new ArrayList();

            var oneWire = new OneWire(Pins.GPIO_PIN_D4);

            var rom = new byte[8];

            if (oneWire.Reset())
            {
                oneWire.WriteByte(OneWire.ReadRom);
                oneWire.Read(rom);

                var deviation = 0;

                do
                {
                    if ((deviation = oneWire.Search(rom, deviation)) == -1)
                    {
                        break;
                    }

                    if (OneWire.ComputeCRC(rom, count: 7) == rom[7])
                    {
                        if (rom[0] == DS18B20.FamilyCode)
                        {
                            var temperatureData = new TemperatureData();

                            oneWire.Reset();
                            oneWire.WriteByte(OneWire.SkipRom);
                            oneWire.WriteByte(DS18B20.ConvertT);

                            Thread.Sleep(750);

                            var matchRom = new byte[9];

                            Array.Copy(rom, 0, matchRom, 1, 8);

                            matchRom[0] = OneWire.MatchRom;

                            oneWire.Reset();
                            oneWire.Write(matchRom);
                            oneWire.WriteByte(DS18B20.ReadScratchpad);

                            var tempLo = oneWire.ReadByte();
                            var tempHi = oneWire.ReadByte();

                            temperatureData.SetValue(DS18B20.GetTemperature(tempLo, tempHi));

                            temperatureData.SetExtValue(OneWireExtensions.BytesToHexString(rom));

                            temperatures.Add(temperatureData);
                        }
                    }
                }
                while (deviation > 0);
            }

            oneWire.Dispose();

            return temperatures;
        }

        public override void TimerCallback(object state)
        {
            //Timer Callbacks receive a Delegate in the state object
            var ida = (InputDataAvailable)state;

            foreach (TemperatureData temperatureData in GetTemperatures())
            {
                // call out to the delegate with expected value
                if (temperatureData.GetValue() > 0)
                {
                    ida(temperatureData);
                }
            }
        }

        #endregion

        #region Methods

        private static void Shell_OnCommandReceived(ShellCore Shell, string[] Arguments, ref bool SuspressError, DateTime Time)
        {
            switch (Arguments[0].ToUpper())
            {
                case "TEMP":
                    foreach (TemperatureData temperatureData in GetTemperatures())
                    {
                        if (temperatureData.GetValue() > 0)
                        {
                            Shell.TelnetServer.Print("Temperature(" + temperatureData.GetExtValue() + "): " + temperatureData.GetValue());
                        }
                    }
                    SuspressError = true;
                    break;
            }
        }

        /// <summary>
        /// Shows a specific help page
        /// </summary>
        /// <param name="Server">The telnet server object</param>
        /// <param name="Page">The page</param>
        /// <returns>True when the page exists</returns>
        private static bool DoHelp(TelnetServer Server, string Page)
        {
            switch (Page)
            {
                case "":
                    Server.Print("TEMP                 Displays current temperatures");
                    return true;
                default:
                    return false;
            }
        }

        #endregion
    }
}