namespace AquariumWiseController.Core.Extensions
{
    using System;
    using System.Net;
    using System.Net.Sockets;

    public class NTP
    {
        #region Public Methods

        public static DateTime GetDateTime(string timeServer, int utcOffset)
        {
            var ep = new IPEndPoint(Dns.GetHostEntry(timeServer).AddressList[0], 123);

            var s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
                {
                    SendTimeout = 5000, 
                    ReceiveTimeout = 5000
                };

            s.Connect(ep);

            var ntpData = new byte[48];
            Array.Clear(ntpData, 0, 48);

            ntpData[0] = 0x1B;

            s.Send(ntpData);
            s.Receive(ntpData);

            const byte OffsetTransmitTime = 40;

            ulong intpart = 0;
            ulong fractpart = 0;

            for (var i = 0; i <= 3; i++)
            {
                intpart = (intpart << 8) | ntpData[OffsetTransmitTime + i];
            }

            for (var i = 4; i <= 7; i++)
            {
                fractpart = (fractpart << 8) | ntpData[OffsetTransmitTime + i];
            }

            var milliseconds = (intpart * 1000 + (fractpart * 1000) / 0x100000000L);

            s.Close();

            var timeSpan = TimeSpan.FromTicks((long)milliseconds * TimeSpan.TicksPerMillisecond);
            var dateTime = new DateTime(1900, 1, 1);

            dateTime += timeSpan;

            var offsetAmount = new TimeSpan(0, utcOffset, 0, 0, 0);
            var networkDateTime = (dateTime + offsetAmount);

            return networkDateTime;
        }

        #endregion
    }
}