namespace System.Diagnostics
{
    using Microsoft.SPOT;

    public class Debug
    {
        #region Public Methods

        [Conditional("TINYCLR_TRACE")]
        public static void WriteLine(string text)
        {
            Trace.Print(text);
        }

        #endregion
    }
}