using System;

namespace AquariumWiseController.Core.Extensions
{
    using System.Diagnostics;

    using AquariumWiseController.Core.Hardware;

    public class TimeEx
    {
        /// <summary>
        /// Determine the timespan from 'now' when the given command should be run
        /// </summary>
        /// <param name="time">An ISO8601 formatted time value representing the time of day for execution</param>
        /// <param name="missedExecute">Mark that run was missed</param>
        /// <returns>Timespan when task should be run</returns>		
        public static TimeSpan GetTimeSpan(string time, out bool missedExecute)
        {
            // Determine when this event should be fired as a TimeSpan
            // Assuming ISO8601 time format "hh:mm"
            var hours = int.Parse(time.Substring(0, 2));
            var minutes = int.Parse(time.Substring(3, 2));
            var now = DateTime.Now;
            var timeToRun = new DateTime(now.Year, now.Month, now.Day, hours, minutes, 0);

            // we missed the window, so setup for next run
            // Also, mark that we missed the run, so we can execute the command during the parse.

            if (timeToRun < now)
            {
                timeToRun = timeToRun.AddDays(1);
                missedExecute = true;
            }
            else
            {
                missedExecute = false;
            }

            return new TimeSpan((timeToRun - now).Ticks);
        }

        public static void SetControllerTime()
        {
            var netTime = new DateTime();

            try
            {
                netTime = NTP.GetDateTime("ntp.optus.net", 10);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR - SetControllerTime - " + ex.Message);
            }

            var rtc = new DS1307();

            if (netTime != DateTime.MinValue)
            {
                rtc.SetTime(netTime);
            }

            rtc.Synchronize();
        }
    }
}
