namespace AquariumWiseController.Core.Extensions
{
    public class BitConverter
    {
        public static short ToInt16(byte[] value, int index = 0)
        {
            return (short)(
                value[0 + index] << 0 |
                value[1 + index] << 8);
        }
    }
}
