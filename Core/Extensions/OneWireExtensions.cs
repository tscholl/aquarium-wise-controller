#region License

//---------------------------------------------------------------------------
//<copyright file="OneWireExtensions.cs">
//
// Copyright 2010 Stanislav "CW" Simicek
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//</copyright>
//---------------------------------------------------------------------------

#endregion

namespace AquariumWiseController.Core.Extensions
{
    /// <summary>
    /// 1-Wire extension and helper methods
    /// </summary>
    public static class OneWireExtensions
    {
        #region Constants and Fields

        private const string HexDigits = "0123456789ABCDEF";

        #endregion

        #region Public Methods

        public static string BytesToHexString(byte[] buffer)
        {
            var chars = new char[buffer.Length * 2];
            for (int i = buffer.Length - 1, c = 0; i >= 0; i--)
            {
                chars[c++] = HexDigits[(buffer[i] & 0xF0) >> 4];
                chars[c++] = HexDigits[(buffer[i] & 0x0F)];
            }
            return new string(chars);
        }

        #endregion
    }
}