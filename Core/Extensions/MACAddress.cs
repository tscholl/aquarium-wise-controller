namespace AquariumWiseController.Core.Extensions
{
    using Microsoft.SPOT.Net.NetworkInformation;

    public static class MACAddress
    {
        #region Public Methods

        public static string Get()
        {
            // Get MAC Address
            NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

            string macAddress = "";

            // Create a character array for hexidecimal conversion.
            const string HexChars = "0123456789ABCDEF";

            // Loop through the bytes.
            for (int b = 0; b < 6; b++)
            {
                // Grab the top 4 bits and append the hex equivalent to the return string.
                macAddress += HexChars[allNetworkInterfaces[0].PhysicalAddress[b] >> 4];

                // Mask off the upper 4 bits to get the rest of it.
                macAddress += HexChars[allNetworkInterfaces[0].PhysicalAddress[b] & 0x0F];

                // Add the dash only if the MAC address is not finished.
                if (b < 5)
                {
                    macAddress += "-";
                }
            }

            return macAddress;
        }

        #endregion
    }
}