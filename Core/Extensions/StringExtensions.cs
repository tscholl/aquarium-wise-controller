#region License

// Copyright (c) 2010 Ross McDermott
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

#endregion

namespace AquariumWiseController.Core.Extensions
{
    /// <summary>
    /// General string extensions
    /// </summary>
    public static class StringExtensions
    {
        #region Public Methods

        /// <summary>
        /// Returns a new string of a specified length in which the beginning of the current string is padded with spaces or with a specified character.
        /// </summary>
        /// <param name="original"></param>
        /// <param name="totalWidth">The number of characters in the resulting string, equal to the number of original characters plus any additional padding characters.</param>
        /// <param name="paddingChar">The padding character.</param>
        /// <returns></returns>
        public static string PadLeft(this string original, int totalWidth, char paddingChar = ' ')
        {
            return PadHelper(original, totalWidth, paddingChar, true);
        }

        /// <summary>
        /// Replace all occurances of the 'find' string with the 'replace' string.
        /// </summary>
        /// <param name="content">Original string to operate on</param>
        /// <param name="find">String to find within the original string</param>
        /// <param name="replace">String to be used in place of the find string</param>
        /// <returns>Final string after all instances have been replaced.</returns>
        public static string Replace(this string content, string find, string replace)
        {
            int startFrom = 0;
            int findItemLength = find.Length;

            int firstFound = content.IndexOf(find, startFrom);
            var returning = new StringBuilder();

            string workingString = content;

            while ((firstFound = workingString.IndexOf(find, startFrom)) >= 0)
            {
                returning.Append(workingString.Substring(0, firstFound));
                returning.Append(replace);

                // the remaining part of the string.
                workingString = workingString.Substring(
                    firstFound + findItemLength, workingString.Length - (firstFound + findItemLength));
            }

            returning.Append(workingString);

            return returning.ToString();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Private method to do the actual padding.
        /// </summary>
        /// <param name="original"></param>
        /// <param name="totalWidth"></param>
        /// <param name="paddingChar"></param>
        /// <param name="appendLeft"></param>
        /// <returns></returns>
        private static string PadHelper(string original, int totalWidth, char paddingChar, bool appendLeft)
        {
            // this seems to be quicker than a simple while (length < totalWidth) pad
            if (original.Length >= totalWidth)
            {
                return original;
            }

            var result = new StringBuilder(totalWidth);

            int padCount = totalWidth - original.Length;

            for (int i = 0; i < padCount; i++)
            {
                result.Append(paddingChar);
            }

            if (appendLeft)
            {
                return result + original;
            }

            return original + result;
        }

        #endregion
    }
}