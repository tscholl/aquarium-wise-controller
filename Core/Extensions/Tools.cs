/*
 * Copyright 2011-2013 Stefan Thoolen (http://www.netmftoolbox.com/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AquariumWiseController.Core.Extensions
{
    /// <summary>
    /// Generic, useful tools
    /// </summary>
    public static class Tools
    {
        #region Constants and Fields

        /// <summary>Contains the name of the hardware provider</summary>
        private static string _HardwareProvider = "";

        #endregion

        #region Public Properties

        /// <summary>
        /// Returns the name of the hardware provider
        /// </summary>
        public static string HardwareProvider
        {
            get
            {
                if (_HardwareProvider == "")
                {
                    string[] HP = Microsoft.SPOT.Hardware.HardwareProvider.HwProvider.ToString().Split(new[] { '.' });
                    _HardwareProvider = HP[HP.Length - 2];
                }
                return _HardwareProvider;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Converts a byte array to a char array
        /// </summary>
        /// <param name="Input">The byte array</param>
        /// <returns>The char array</returns>
        public static char[] Bytes2Chars(byte[] Input)
        {
            var Output = new char[Input.Length];
            for (int Counter = 0; Counter < Input.Length; ++Counter)
            {
                Output[Counter] = (char)Input[Counter];
            }
            return Output;
        }

        /// <summary>
        /// Converts an 8-bit array to a 16 bit array
        /// </summary>
        /// <param name="Data">The 8-bit array</param>
        /// <returns>The 16-bit array</returns>
        public static ushort[] BytesToUShorts(byte[] Data)
        {
            var RetVal = new ushort[Data.Length / 2];

            int BytePos = 0;
            for (int ShortPos = 0; ShortPos < RetVal.Length; ++ShortPos)
            {
                RetVal[ShortPos] = (ushort)((Data[BytePos++] << 8) + Data[BytePos++]);
            }
            return RetVal;
        }

        /// <summary>
        /// Converts a char array to a byte array
        /// </summary>
        /// <param name="Input">The char array</param>
        /// <returns>The byte array</returns>
        public static byte[] Chars2Bytes(char[] Input)
        {
            var Output = new byte[Input.Length];
            for (int Counter = 0; Counter < Input.Length; ++Counter)
            {
                Output[Counter] = (byte)Input[Counter];
            }
            return Output;
        }

        /// <summary>
        /// Converts a number to a Hex string
        /// </summary>
        /// <param name="Input">The number</param>
        /// <param name="MinLength">The minimum length of the return string (filled with 0s)</param>
        /// <returns>The Hex string</returns>
        public static string Dec2Hex(int Input, int MinLength = 0)
        {
            // Contains all Hex posibilities
            string ConversionTable = "0123456789ABCDEF";
            // Starts the conversion
            string RetValue = "";
            int Current = 0;
            int Next = Input;
            do
            {
                if (Next >= ConversionTable.Length)
                {
                    // The current digit
                    Current = (Next / ConversionTable.Length);
                    if (Current * ConversionTable.Length > Next)
                    {
                        --Current;
                    }
                    // What's left
                    Next = Next - (Current * ConversionTable.Length);
                }
                else
                {
                    // The last digit
                    Current = Next;
                    // Nothing left
                    Next = -1;
                }
                RetValue += ConversionTable[Current];
            }
            while (Next != -1);

            return ZeroFill(RetValue, MinLength);
        }

        /// <summary>
        /// Converts a Hex string to a number
        /// </summary>
        /// <param name="HexNumber">The Hex string (ex.: "0F")</param>
        /// <returns>The decimal value</returns>
        public static uint Hex2Dec(string HexNumber)
        {
            // Always in upper case
            HexNumber = HexNumber.ToUpper();
            // Contains all Hex posibilities
            string ConversionTable = "0123456789ABCDEF";
            // Will contain the return value
            uint RetVal = 0;
            // Will increase
            uint Multiplier = 1;

            for (int Index = HexNumber.Length - 1; Index >= 0; --Index)
            {
                RetVal += (uint)(Multiplier * (ConversionTable.IndexOf(HexNumber[Index])));
                Multiplier = (uint)(Multiplier * ConversionTable.Length);
            }

            return RetVal;
        }

        /// <summary>
        /// Displays a number with a metric prefix
        /// </summary>
        /// <param name="Number">The number</param>
        /// <param name="BinaryMultiple">If true, will use 1024 as multiplier instead of 1000</param>
        /// <returns>The number with a metric prefix</returns>
        /// <remarks>See also: http://en.wikipedia.org/wiki/Metric_prefix </remarks>
        public static string MetricPrefix(float Number, bool BinaryMultiple = false)
        {
            float Multiplier = BinaryMultiple ? 1024 : 1000;
            if (Number > (Multiplier * Multiplier * Multiplier * Multiplier))
            {
                return Round(Number / Multiplier / Multiplier / Multiplier / Multiplier) + "T";
            }
            if (Number > (Multiplier * Multiplier * Multiplier))
            {
                return Round(Number / Multiplier / Multiplier / Multiplier) + "G";
            }
            if (Number > (Multiplier * Multiplier))
            {
                return Round(Number / Multiplier / Multiplier) + "M";
            }
            if (Number > Multiplier)
            {
                return Round(Number / Multiplier) + "k";
            }
            else
            {
                return Round(Number);
            }
        }

        /// <summary>
        /// Rounds a value to a certain amount of digits
        /// </summary>
        /// <param name="Input">The input number</param>
        /// <param name="Digits">Amount of digits after the .</param>
        /// <returns>The rounded value (as float or double gave precision errors, hence the String type)</returns>
        public static string Round(float Input, int Digits = 2)
        {
            int Multiplier = 1;
            for (int i = 0; i < Digits; ++i)
            {
                Multiplier *= 10;
            }
            string Rounded = ((int)(Input * Multiplier)).ToString();

            return
                (Rounded.Substring(0, Rounded.Length - 2) + "." + Rounded.Substring(Rounded.Length - 2)).TrimEnd(
                    new[] { '0', '.' });
        }

        /// <summary>
        /// Converts a 16-bit array to an 8 bit array
        /// </summary>
        /// <param name="Data">The 16-bit array</param>
        /// <returns>The 8-bit array</returns>
        public static byte[] UShortsToBytes(ushort[] Data)
        {
            var RetVal = new byte[Data.Length * 2];

            int BytePos = 0;
            for (int ShortPos = 0; ShortPos < Data.Length; ++ShortPos)
            {
                RetVal[BytePos++] = (byte)(Data[ShortPos] >> 8);
                RetVal[BytePos++] = (byte)(Data[ShortPos] & 0x00ff);
            }
            return RetVal;
        }

        /// <summary>
        /// Changes a number into a string and add zeros in front of it, if required
        /// </summary>
        /// <param name="Number">The input number</param>
        /// <param name="Digits">The amount of digits it should be</param>
        /// <param name="Character">The character to repeat in front (default: 0)</param>
        /// <returns>A string with the right amount of digits</returns>
        public static string ZeroFill(string Number, int Digits, char Character = '0')
        {
            bool Negative = false;
            if (Number.Substring(0, 1) == "-")
            {
                Negative = true;
                Number = Number.Substring(1);
            }

            for (int Counter = Number.Length; Counter < Digits; ++Counter)
            {
                Number = Character + Number;
            }
            if (Negative)
            {
                Number = "-" + Number;
            }
            return Number;
        }

        #endregion
    }
}