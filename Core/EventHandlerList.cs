namespace AquariumWiseController.Core
{
    using System;
    using System.Collections;

    /// <summary>
    /// Collection to hold a list of keyed Delegates.  Allows registering
    /// multiple Handlers against one type of Event
    /// </summary>
    public sealed class EventHandlerList : IDisposable
    {
        #region Constants and Fields

        private readonly Object m_lock;

        private Hashtable m_table;

        #endregion

        #region Constructors and Destructors

        public EventHandlerList()
        {
            this.m_lock = new Object();
        }

        #endregion

        #region Public Indexers

        public Delegate this[object _key]
        {
            get
            {
                if (this.m_table == null)
                {
                    return null;
                }
                return this.m_table[_key] as Delegate;
            }
            set
            {
                this.AddHandler(_key, value);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds a delegate to the list
        /// </summary>
        /// <param name="_key">Key to identify the Handler</param>
        /// <param name="_value">Delegate to store</param>
        public void AddHandler(object _key, Delegate _value)
        {
            lock (this.m_lock)
            {
                if (this.m_table == null)
                {
                    this.m_table = new Hashtable();
                }

                var prev = this.m_table[_key] as Delegate;
                prev = Delegate.Combine(prev, _value);
                this.m_table[_key] = prev;
            }
        }

        public void Dispose()
        {
            this.m_table = null;
        }

        /// <summary>
        /// Removes a delegate from the list
        /// </summary>
        /// <param name="_key">Key of handler to remove</param>
        /// <param name="_value">Delegate to remove from list</param>
        public void RemoveHandler(object _key, Delegate _value)
        {
            if (this.m_table == null)
            {
                return;
            }

            lock (this.m_lock)
            {
                var prev = this.m_table[_key] as Delegate;
                prev = Delegate.Remove(prev, _value);
                this.m_table[_key] = prev;
            }
        }

        #endregion
    }
}