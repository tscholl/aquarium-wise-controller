#region License

/*
 * Copyright 2011-2013 Stefan Thoolen (http://www.netmftoolbox.com/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#endregion

namespace AquariumWiseController.Core.Hardware
{
    using Microsoft.SPOT.Hardware;

    /// <summary>
    /// SPI Helper to make it easier to use multiple SPI-devices on one SPI-bus
    /// </summary>
    public class MultiSPI
    {
        #region Constants and Fields

        /// <summary>SPI Configuration. Different for each device, so not a static reference</summary>
        private readonly SPI.Configuration _Configuration;

        /// <summary>Reference to the SPI Device. All MultiSPI devices use the same SPI class from the NETMF, so this reference is static</summary>
        private static SPI _SPIDevice;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new SPI device
        /// </summary>
        /// <param name="config">The SPI-module configuration</param>
        public MultiSPI(SPI.Configuration config)
        {
            // Sets the configuration in a local value
            this._Configuration = config;

            // If no SPI Device exists yet, we create it's first instance
            if (_SPIDevice == null)
            {
                // Creates the SPI Device
                _SPIDevice = new SPI(this._Configuration);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// The 8-bit bytes to write to the SPI-buffer
        /// </summary>
        /// <param name="WriteBuffer">An array of 8-bit bytes</param>
        public void Write(byte[] WriteBuffer)
        {
            _SPIDevice.Config = this._Configuration;
            _SPIDevice.Write(WriteBuffer);
        }

        #endregion
    }
}