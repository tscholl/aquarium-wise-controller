﻿namespace AquariumWiseController.Core.Hardware
{
    using System;
    using System.Threading;

    using AquariumWiseController.Core.Extensions;

    internal class BH1750
    {
        #region Constants and Fields

        /// <summary>
        /// Reference to the I2C bus
        /// </summary>
        private readonly MultiI2C _Device;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initialises a new BH1750 Module
        /// </summary>
        /// <param name="Address">The I2C address</param>
        /// <param name="ClockRateKhz">The module speed in Khz</param>
        public BH1750(byte Address = 0x23, int ClockRateKhz = 400)
        {
            this._Device = new MultiI2C(Address, ClockRateKhz);

            this._Device.Write(new byte[] { 0x10 } );

            Thread.Sleep(130);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the Lux from the module
        /// </summary>
        /// <returns>The current time</returns>
        public double GetLux()
        {
            var readBuffer = new byte[2];

            int bytesRead = this._Device.Read(readBuffer);
            if (bytesRead != 2)
            {
                throw new ApplicationException("Something went wrong reading Lux.");
            }

            return ((readBuffer[0] << 8) | readBuffer[1]) / 1.2;
        }

        #endregion
    }
}