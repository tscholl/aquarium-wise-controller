#region License

/*
 * Copyright 2012-2013 Stefan Thoolen (http://www.netmftoolbox.com/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#endregion

namespace AquariumWiseController.Core.Hardware
{
    using System;

    using Microsoft.SPOT.Hardware;

    /// <summary>
    /// A chain of one or multiple serial to parallel ICs over managed SPI
    /// </summary>
    public class Ic74hc595 : IDisposable
    {
        #region Constants and Fields

        /// <summary>Reference to all pins</summary>
        public IGPOPort[] Pins;

        /// <summary>A reference to the SPI Interface</summary>
        private readonly MultiSPI _SpiInterface;

        /// <summary>Contains all pin values</summary>
        private byte[] _Data;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initialises a chain of one or multiple serial to parallel ICs over managed SPI
        /// </summary>
        /// <param name="SPI_Module">The SPI interface it's connected to</param>
        /// <param name="LatchPin">The slave select pin connected to the IC(s)</param>
        /// <param name="Bytes">The amount of 8-bit IC(s) connected</param>
        /// <param name="SpeedKHz">The max. SPI speed</param>
        public Ic74hc595(SPI.SPI_module SPI_Module, Cpu.Pin LatchPin, uint Bytes = 1, uint SpeedKHz = 1000)
        {
            // Full SPI configuration
            this._SpiInterface =
                new MultiSPI(
                    new SPI.Configuration(
                        ChipSelect_Port: LatchPin,
                        ChipSelect_ActiveState: false,
                        ChipSelect_SetupTime: 0,
                        ChipSelect_HoldTime: 0,
                        Clock_IdleState: true,
                        Clock_Edge: true,
                        Clock_RateKHz: SpeedKHz,
                        SPI_mod: SPI_Module));

            // The amount of ICs
            this._Init(Bytes);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Disposes this object, frees all locked items
        /// </summary>
        public void Dispose()
        {
            // Turns off all pins
            for (uint ByteNo = 0; ByteNo < this._Data.Length; ++ByteNo)
            {
                this._Data[ByteNo] = 0;
            }
            this._WriteSPI();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Writes a single bit
        /// </summary>
        /// <param name="Bit">The bit to write</param>
        /// <param name="State">The new state for the bit</param>
        protected void Write(uint Bit, bool State)
        {
            // Writes the bit
            this._Write(Bit, State);
            // Actually sends it to the SPI bus
            this._WriteSPI();
        }

        /// <summary>
        /// Writes a byte to the buffer
        /// </summary>
        /// <param name="StartBit">The first bit to write</param>
        /// <param name="Data">The data to write</param>
        /// <param name="BitCount">The amount of bits to write</param>
        /// <param name="Inverted">When true, bits will be inverted</param>
        protected void WriteByte(uint StartBit, uint Data, int BitCount, bool Inverted = false)
        {
            for (int BitNo = BitCount - 1; BitNo >= 0; --BitNo)
            {
                int BitMask = 1 << BitNo;
                bool Value = (Data & BitMask) == BitMask;
                uint PinNo;
                if (Inverted)
                {
                    PinNo = (uint)(StartBit + BitNo);
                }
                else
                {
                    PinNo = (uint)(StartBit + BitCount - 1 - BitNo);
                }
                this._Write(PinNo, Value);
            }
            this._WriteSPI();
        }

        /// <summary>
        /// Initialises all bits and bytes
        /// </summary>
        /// <param name="Bytes">The amount of 8-bit IC(s) connected</param>
        private void _Init(uint Bytes)
        {
            // Prepares the data array
            this._Data = new byte[Bytes];

            // Declares all pins
            this.Pins = new Ic74hc595GPOPort[Bytes * 8];
            for (uint PinNo = 0; PinNo < this.Pins.Length; ++PinNo)
            {
                this.Pins[PinNo] = new Ic74hc595GPOPort(this, PinNo);
            }

            // Writes all zeroes to the SPI bus for the first time
            this._WriteSPI();
        }

        /// <summary>
        /// Writes a single bit to the buffer
        /// </summary>
        /// <param name="Bit">The bit to write</param>
        /// <param name="State">The new state for the bit</param>
        private void _Write(uint Bit, bool State)
        {
            // Calculates the actual bit and actual byte
            var BitNo = (byte)(Bit & 0x07);
            uint ByteNo = (uint)this._Data.Length - 1 - ((Bit - BitNo) / 8);
            var BitMask = (byte)(1 << (7 - BitNo));
            // Now we'll apply the new state
            if ((this._Data[ByteNo] & BitMask) == BitMask && State == false)
            {
                this._Data[ByteNo] -= BitMask;
            }
            else if ((this._Data[ByteNo] & BitMask) != BitMask && State)
            {
                this._Data[ByteNo] += BitMask;
            }
        }

        /// <summary>
        /// Pushes all data to the SPI bus
        /// </summary>
        private void _WriteSPI()
        {
            this._SpiInterface.Write(this._Data);
        }

        #endregion

        /// <summary>GPO Port wrapper for the SPIShifterOut class</summary>
        protected class Ic74hc595GPOPort : IGPOPort
        {
            #region Constants and Fields

            /// <summary>The number of the bit</summary>
            private readonly uint _BitNo;

            /// <summary>Reference to the main chain</summary>
            private readonly Ic74hc595 _Chain;

            #endregion

            #region Constructors and Destructors

            /// <summary>
            /// Defines a GPO Port
            /// </summary>
            /// <param name="MainChain">The object of the main chain</param>
            /// <param name="BitNo">The number of the bit</param>
            public Ic74hc595GPOPort(Ic74hc595 MainChain, uint BitNo)
            {
                // Copies the parameters to local values
                this._Chain = MainChain;
                this._BitNo = BitNo;
            }

            #endregion

            #region Public Properties

            /// <summary>True when the pin is high, false when low</summary>
            public bool State { get; protected set; }

            #endregion

            #region Public Methods

            /// <summary>
            /// Frees the pin
            /// </summary>
            public void Dispose()
            {
            }

            /// <summary>
            /// Writes the pin value
            /// </summary>
            /// <param name="State">True for high, false for low</param>
            public void Write(bool State)
            {
                this._Chain.Write(this._BitNo, State);
                this.State = State;
            }

            #endregion
        }
    }
}