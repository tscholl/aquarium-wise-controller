Aquarium Wise Controller
========================
Netduino Aquarium Controller

Sensors:

- Temperature via DS18B20 OneWire
- pH via [Atlas Scientific pH Stamp](http://atlas-scientific.com/product_pages/embedded/ph.html)
- Lux via bh1750

Logging and Display:

- Logging to file on SD card
- Logging to [aquariumwise.net](http://www.aquariumwise.net) (example [http://my.aquariumwise.net/1](http://my.aquariumwise.net/1))
- Display to Hd44780 LCD

Control:

- Multi-channel power relays via SPI & 74HC595
- PWM Lighting control

Credit
------

- Plugin & scheduling code is heavily based on h07r0d's [Netduino-Aquarium-Controller](https://github.com/h07r0d/Netduino-Aquarium-Controller)
- Lots of time saved by the source in [.NET Micro Framework Toolbox](http://netmftoolbox.codeplex.com/)
- [OneWire ALPHA Firmware](http://forums.netduino.com/index.php?/topic/230-onewire-alpha/) thanks to CW2